import * as React from 'react';
import styles from '../styles/checkout-detail.module.scss';
import AppBar from '@mui/material/AppBar';
import Avatar from '@mui/material/Avatar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import { ArrowBack } from '@mui/icons-material';
import Router from 'next/router'


export default function HeadBar({ studentData, showBackButton }) {

  return (
    <AppBar
      sx={{
        color: '#333',
        backgroundColor: 'white',
      }}
    >
      <Toolbar
        sx={{
          justifyContent: 'center'
        }}
      >
        {showBackButton && (
          <div
            onClick={() => Router.back()}
          >
            <IconButton
              className={styles.backButton}
              sx={{ padding: "0px" }}
            >
              <ArrowBack />
            </IconButton>
          </div>
        )}
        <Avatar
          sx={{ bgcolor: '#828282', marginRight: '12px' }}
        >
          <Typography
            sx={{ fontSize: '20px', fontWeight: 900 }}
          >
            {studentData.school.name[0]}
          </Typography>
        </Avatar>
        <Typography variant="h6" component="div"
          className={styles.schoolName}
        >
          {studentData.school.name}
        </Typography>
      </Toolbar>
    </AppBar>
  );
}
