import * as React from 'react';
import styles from '../styles/checkout-detail.module.scss';
import ExpandMore from './expand-more';

import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Delete } from '@mui/icons-material';


export default function CheckoutDetail({ order, showDeleteButton, deleteOrder }) {
  const [expanded, setExpanded] = React.useState(false);

  return (
    <div className={styles.paymentsItem}>
      <div className={styles.paymentsItemHeader}>
        <div className={styles.paymentsItemDelete}>
          {showDeleteButton && (
            <IconButton
              sx={{ padding: "0px" }}
              onClick={deleteOrder}
            >
              <Delete />
            </IconButton>
          )}
        </div>
        <div>{order.name}</div>
        <div className={styles.paymentsItemAmount}>{order.price + Number(order.interest)} {order.price_currency}</div>
        <div className={styles.paymentsItemShowMore}>
          <ExpandMore
            expand={expanded}
            onClick={() => setExpanded(!expanded)}
            aria-expanded={expanded}
            aria-label="show more"
            sx={{ padding: "0px" }}
          >
            <ExpandMoreIcon className={styles.expandMoreIcon} />
          </ExpandMore>
        </div>
      </div>
      <Collapse in={expanded} timeout="auto" unmountOnExit>

        {/* Mocked data */}
        <div className={styles.paymentsItemDetails}>
          <div className={styles.paymentsItemDetail}>
            <div></div>
            <div>Colegiatura del periodo</div>
            <div className={styles.paymentsItemAmount}>$1400</div>
            <div></div>
          </div>
          <div className={styles.paymentsItemDetail}>
            <div></div>
            <div>Transportación</div>
            <div className={styles.paymentsItemAmount}>$150</div>
            <div></div>
          </div>
          <div className={styles.paymentsItemDetail}>
            <div></div>
            <div>Extracurriculares</div>
            <div className={styles.paymentsItemAmount}>$450</div>
            <div></div>
          </div>
          <div className={styles.paymentsItemDetail}>
            <div></div>
            <div>Interés</div>
            <div className={styles.paymentsItemAmount}>$200</div>
            <div></div>
          </div>
        </div>
      </Collapse>
    </div>
  );
}
