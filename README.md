
## Run Project

1. Build the images:

    ```sh
    $ docker-compose build
    ```
2. Run development project:

    ```sh
    $ docker-compose up
    ```
3. Go to 

    ```sh
    localhost:3000/checkout-status
    ```

