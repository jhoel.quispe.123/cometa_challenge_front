import * as React from 'react';
import styles from '../styles/checkout-status.module.scss';
import { getStudentData, getStudentOrders } from '../api/common'
import ExpandMore from '../components/expand-more';
import HeadBar from '../components/head-bar';

import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';
import Collapse from '@mui/material/Collapse';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Checkbox from '@mui/material/Checkbox';
import Link from 'next/link'


export default function CheckoutStatus({ orders, studentData }) {
  const [expanded, setExpanded] = React.useState(false);
  const [expandedDue, setExpandedDue] = React.useState(true)
  const [expandedNext, setExpandedNext] = React.useState(true);
  const [selectedPayments, setSelectedPayments] = React.useState(0);
  const [totalAmount, setTotalAmount] = React.useState(0);

  const handleChangeCheckbox = (e, order) => {
    setSelectedPayments(e.target.checked ? selectedPayments + 1 : selectedPayments - 1)
    const price = order.price + order.interest;
    if (e.target.checked) {
      setTotalAmount(totalAmount + price)
    } else {
      setTotalAmount(totalAmount - price)
    }
  }

  return (
    <div className={styles.checkoutStatusPage}>
      <CssBaseline />
      <HeadBar studentData={studentData} showBackButton={false}/>
      <Toolbar />
      <Container
      >
        <Paper
          sx={{ marginTop: '15px', marginBottom: '12px' }}
          className={styles.studentSection}
        >
          <div className={styles.studentName}>{studentData.first_name} {studentData.last_name}</div>
          <div className={styles.studentYear}>{studentData.cohort}</div>
          <div className={styles.totalAmountLabel}>Total a Pagar</div>
          <div className={styles.totalAmount}>$ {totalAmount}</div>
        </Paper>

        <Paper
          className={styles.paymentsSection}
          sx={{ marginBottom: '15px' }}
        >
          <div className={styles.paymentsSectionHeader}>
            <div>
              <div className={styles.paymentsTitle}>
                Cuotas Pagadas
              </div>
              <div className={styles.paymentsSubtitle}>Dale click para expandir</div>
            </div>
            <ExpandMore
              expand={expanded}
              onClick={() => setExpanded(!expanded)}
              aria-expanded={expanded}
              aria-label="show more"
            >
              <ExpandMoreIcon className={styles.exapandMoreIcon} />
            </ExpandMore>
          </div>
          <Collapse in={expanded} timeout="auto" unmountOnExit>
            {orders.paidOrders.map((order) => (
              <div className={styles.paymentItem} key={order.id}>
                <div className={styles.paymentItemData}>
                  <div>
                    <Typography sx={{ fontSize: '15px', lineHeight: '18px' }} >
                      {order.name}
                    </Typography>
                    <Typography sx={{ fontSize: '10px' }} >
                      Pagado el {order.due}
                    </Typography>
                  </div>
                  <div className={styles.paymentItemDataAmount}>
                    <Typography sx={{ fontSize: '15px', lineHeight: '18px' }} >
                      {order.price} {order.price_currency}
                    </Typography>
                    {(order.interest &&
                      <Typography sx={{ fontSize: '10px' }} >
                        Interés: {order.interest}
                      </Typography>
                    )}
                  </div>
                </div>
              </div>
            ))}
          </Collapse>
        </Paper>

        <Paper
          className={styles.paymentsSection}
          sx={{ marginBottom: '27px' }}
        >
          <div className={styles.paymentsSectionHeader}>
            <div>
              <div className={styles.paymentsTitle}>
                Cuotas pendientes
              </div>
              <div className={styles.paymentsSubtitle}>Puedes pagar más de uno</div>
            </div>
            <ExpandMore
              expand={expandedDue}
              onClick={() => setExpandedDue(!expandedDue)}
              aria-label="show more"
            >
              <ExpandMoreIcon className={styles.expandMoreIcon} />
            </ExpandMore>
          </div>
          <Collapse in={expandedDue} timeout="auto" unmountOnExit>

            {orders.dueOrders.map((order, index) => (
              <div className={styles.paymentItem} key={order.id}>
                <div className={styles.paymentItemData}>
                  <div>
                    <Typography sx={{ fontSize: '15px', lineHeight: '18px' }} >
                      {order.name}
                    </Typography>
                    <Typography sx={{ fontSize: '10px' }} >
                      Vence el {order.due}
                    </Typography>
                  </div>
                  <div className={styles.paymentItemDataAmount}>
                    <Typography sx={{ fontSize: '15px', lineHeight: '18px' }} >
                      {order.price} {order.price_currency}
                    </Typography>
                    <Typography sx={{ fontSize: '10px' }} >
                      Interés: {order.interest}
                    </Typography>
                  </div>
                </div>
                <div>
                  <Checkbox
                    disabled={index > selectedPayments || selectedPayments - 1 > index}
                    sx={{
                      color: '#333',
                      '&.Mui-checked': {
                        color: '#333',
                      },
                    }}
                    onChange={e => handleChangeCheckbox(e, order)}
                  />
                </div>
              </div>
            ))}
          </Collapse>
        </Paper>

        <Paper
          className={styles.paymentsSection}
        >
          <div className={styles.paymentsSectionHeader}>
            <div>
              <div className={styles.paymentsTitle}>
                Cuotas futuras
              </div>
            </div>
            <ExpandMore
              expand={expandedNext}
              onClick={() => setExpandedNext(!expandedNext)}
              aria-label="show more"
              sx={{ padding: "0px" }}
            >
              <ExpandMoreIcon className={styles.expandMoreIcon} />
            </ExpandMore>
          </div>
          <Collapse in={expandedNext} timeout="auto" unmountOnExit>
            {orders.nextOrders.map((order, index) => (
              <div className={styles.paymentItem} key={order.id}>
                <div className={styles.paymentItemData}>
                  <div>
                    <Typography sx={{ fontSize: '15px', lineHeight: '18px' }} >
                      {order.name}
                    </Typography>
                    <Typography sx={{ fontSize: '10px' }} >
                      Vencido el {order.due}
                    </Typography>
                  </div>
                  <div className={styles.paymentItemDataAmount}>
                    <Typography sx={{ fontSize: '15px', lineHeight: '18px' }} >
                      {order.price} {order.price_currency}
                    </Typography>
                    {(order.interest > 0 &&
                      <Typography sx={{ fontSize: '10px' }} >
                        Interés: {order.interest}
                      </Typography>
                    )}
                  </div>
                </div>
                <div>
                  <Checkbox
                    disabled={index + orders.dueOrders.length > selectedPayments || selectedPayments - 1 > index + orders.dueOrders.length}
                    sx={{
                      color: '#333',
                      '&.Mui-checked': {
                        color: '#333',
                      },
                    }}
                    onChange={e => handleChangeCheckbox(e, order)}
                  />
                </div>
              </div>
            ))}
          </Collapse>
        </Paper>

        <Link
          href="/checkout-detail"
        >
          <Button
            variant="contained"
            className={styles.payButton} sx={{
              backgroundColor: '#333', borderRadius: '20px',
              '&:hover': {
                backgroundColor: '#444',
              },
            }}>
            <Typography sx={{ fontSize: '16px', lineHeight: '18px' }} >
              ir a pagar
            </Typography>
          </Button>
        </Link>

      </Container>
    </div>
  );
}

export async function getServerSideProps() {
  const STUDENT_ID = '3b35fb50-3d5e-41b3-96d6-c5566141fab0'
  const studentData = await getStudentData(STUDENT_ID)
  const studentOrders = await getStudentOrders(STUDENT_ID)
  const orders = {
    paidOrders: studentOrders.filter((order) => order.status === 'PAID'),
    dueOrders: studentOrders.filter((order) => order.status === 'DUE'),
    nextOrders: studentOrders.filter((order) => order.status === 'OUTSTANDING'),
  }

  return { props: { orders, studentData } }
}
