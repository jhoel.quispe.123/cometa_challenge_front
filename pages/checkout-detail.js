import * as React from 'react';
import styles from '../styles/checkout-detail.module.scss';
import { getStudentData, getStudentOrders } from '../api/common'
import OrderDetail from '../components/order-detail'
import HeadBar from '../components/head-bar';
import Button from '@mui/material/Button';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import { AddCircleOutline } from '@mui/icons-material';


export default function CheckoutDetail({ orders, studentData }) {
  const [nSelectedPayments, setNSelectedPayments] = React.useState(2);

  return (
    <div className={styles.checkoutDetailPage}>
      <CssBaseline />
      <HeadBar studentData={studentData} showBackButton={true}/>
      <Toolbar />
      <div>
        <section>
          <div className={styles.studentSection}>
            <Typography className={styles.studentName}>{studentData.first_name} {studentData.last_name}</Typography>
            <Typography className={styles.studentYear}>{studentData.cohort}</Typography>
          </div>
        </section>

        <section
          className={styles.paymentsSection}
        >
          <div>
            <Typography
              sx={{ fontWeight: "bold", fontSize: "15px", marginLeft: "22px", marginBottom: "16px" }}
            >
              {nSelectedPayments} {nSelectedPayments > 1 ? 'Pagos Seleccionados' : 'Pago Seleccionado'}
            </Typography>
          </div>
          <div>
            {orders.nextOrders.slice(0, nSelectedPayments).map((order, index) => (
              <OrderDetail
                key={order.id}
                order={order}
                showDeleteButton={nSelectedPayments - 1 === index && nSelectedPayments > 1}
                deleteOrder={() => setNSelectedPayments(nSelectedPayments - 1)}
              />
            ))}
            <div className={styles.addPayment}>
              <div>Agregar otro mes para pagar</div>
              <div className={styles.addPaymentIcon}>
                <IconButton
                  sx={{ padding: "0px" }}
                  onClick={() => setNSelectedPayments(nSelectedPayments + 1)}
                >
                  <AddCircleOutline className={styles.expandMoreIcon}
                  />
                </IconButton>
              </div>
            </div>
          </div>
        </section>

        <section className={styles.confirmAmountSection}>
          <AppBar
            position="fixed"
            sx={{ top: 'auto', bottom: 0, backgroundColor: 'white', height: '180px', color: '#333', textAlign: 'center' }}>
            <Toolbar sx={{ padding: '45px 20px 18px', display: 'block' }}>
              <Box className={styles.amountText}>
                <Typography sx={{ fontWeight: "bold", fontSize: '18px' }}>Total a pagar</Typography>
                <Typography sx={{ fontWeight: "bold", fontSize: '18px' }}>
                  $ {orders.nextOrders.slice(0, nSelectedPayments).reduce((a, b) => a + Number(b.price) + Number(b.interest), 0)}
                </Typography>
              </Box>

              <Button
                variant="contained"
                className={styles.payButton} sx={{
                  backgroundColor: '#333', borderRadius: '20px',
                  '&:hover': {
                    backgroundColor: '#444',
                  },
                }}>
                <Typography sx={{ fontSize: '16px', lineHeight: '18px' }} >
                  ir a pagar
                </Typography>
              </Button>
            </Toolbar>
          </AppBar>
        </section>

      </div >
    </div >
  );
}

export async function getServerSideProps() {
  const STUDENT_ID = '3b35fb50-3d5e-41b3-96d6-c5566141fab0'
  const studentData = await getStudentData(STUDENT_ID)
  const studentOrders = await getStudentOrders(STUDENT_ID)
  const orders = {
    nextOrders: studentOrders.filter((order) => ['DUE', 'OUTSTANDING'].includes(order.status)),
  }
  return { props: { orders, studentData } }
}
