
const API_HOST = 'http://ec2-3-239-221-74.compute-1.amazonaws.com:8000/api/v1'
const API_HASH = 'OcJn4jYChW'

export async function getStudentData(studentId) {
  const STUDENT_DATA_ENDPOINT = API_HOST + `/students/${studentId}/`

  const studentDataResponse = await fetch(STUDENT_DATA_ENDPOINT, { headers: { hash: API_HASH } })
  const studentDataRaw = await studentDataResponse.json()

  return studentDataRaw
}

export async function getStudentOrders(studentId) {
  const STUDENT_ORDERS_ENDPOINT = API_HOST + `/students/${studentId}/orders/`

  const studentOrdersResponse = await fetch(STUDENT_ORDERS_ENDPOINT, { headers: { hash: API_HASH } })
  const studentOrdersRaw = await studentOrdersResponse.json()

  return studentOrdersRaw.map((order) => ({
    ...order,
    interest: order.interest === "None" ? 0 : Number(order.interest),
    price: Number(order.price)
  }))
}
